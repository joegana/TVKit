package org.drl.tvkit.task;

import java.util.concurrent.ThreadFactory;

public class LevelThreadFactory implements ThreadFactory {
	public Thread newThread(Runnable r) {
		return new VooleThread(r);
	}

	private static class VooleThread extends Thread {
		public VooleThread(Runnable r) {
			super(r);
		}
		@Override
		public void run() {
			android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
			try{
			super.run();
			}catch(Exception ex){
				ex.printStackTrace();
				System.gc();
			}
		}
	}
}
