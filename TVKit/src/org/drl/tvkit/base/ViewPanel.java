package org.drl.tvkit.base;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public abstract class ViewPanel {

	private Activity mActivity;

	public void onCreate(Activity _aty) {
		mActivity = _aty;
	}

	public Activity getActivity() {
		return mActivity;
	}

	public Context getContext() {
		return mActivity;
	}

	public abstract View onCreateView(LayoutInflater inflater);

}
