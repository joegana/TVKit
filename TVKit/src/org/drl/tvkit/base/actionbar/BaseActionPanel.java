package org.drl.tvkit.base.actionbar;

import org.drl.tvkit.base.ViewPanel;

public abstract class BaseActionPanel extends ViewPanel {

	private static BaseActionPanel mBaseActionPanel = null;

	@SuppressWarnings("unchecked")
	public static final BaseActionPanel getInstance(String clsName) {
		if (mBaseActionPanel == null) {

			try {
				Class<? extends BaseActionPanel> _class = ((Class<? extends BaseActionPanel>) Class.forName(clsName));
				if (_class != null) {
					mBaseActionPanel = _class.newInstance();
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				mBaseActionPanel = new TvKitActionPanel();
			}

		}
		return mBaseActionPanel;
	}
}
