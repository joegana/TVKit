package org.drl.tvkit.base.actionbar;

import org.drl.tvkit.R;

import android.view.LayoutInflater;
import android.view.View;

public class TvKitActionPanel extends BaseActionPanel {

	private View mView;
	@Override
	public View onCreateView(LayoutInflater inflater) {
		mView = inflater.inflate(R.layout.action_panel_kit, null);
		return mView;
	}

}
