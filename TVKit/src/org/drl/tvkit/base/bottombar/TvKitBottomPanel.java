package org.drl.tvkit.base.bottombar;

import org.drl.tvkit.R;

import android.view.LayoutInflater;
import android.view.View;

public class TvKitBottomPanel extends BaseBottomPanel {

	private View mView;
	@Override
	public View onCreateView(LayoutInflater inflater) {
		mView = inflater.inflate(R.layout.view_pagertabstrip, null);
		return mView;
	}
	

}
