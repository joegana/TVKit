package org.drl.tvkit.base.bottombar;

import org.drl.tvkit.base.ViewPanel;

/**
 * @author Administrator
 *
 */
public abstract class BaseBottomPanel extends ViewPanel {

	private static BaseBottomPanel mBaseActionPanel = null;

	@SuppressWarnings("unchecked")
	public static final BaseBottomPanel getInstance(String clsName) {
		if (mBaseActionPanel == null) {

			try {
				Class<? extends BaseBottomPanel> _class = ((Class<? extends BaseBottomPanel>) Class.forName(clsName));
				if (_class != null) {
					mBaseActionPanel = _class.newInstance();
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				mBaseActionPanel = new TvKitBottomPanel();
			}

		}
		return mBaseActionPanel;
	}
}
