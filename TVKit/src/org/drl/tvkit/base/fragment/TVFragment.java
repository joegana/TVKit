package org.drl.tvkit.base.fragment;

import org.drl.tvkit.util.L;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class TVFragment extends Fragment{
	
	private String tvTag;
	private boolean mClipChildren = Boolean.TRUE;
	public TVFragment() {
		super();
		tvTag = getClass().getSimpleName();
	}
	public String getTvTag() {
		return tvTag;
	}
	public void setTvTag(String tvTag) {
		this.tvTag = tvTag;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	@Override
	public void onResume() {
		super.onResume();
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	private String mTitleName = getClass().getSimpleName();
	public String getTitleName() {
		return mTitleName;
	}
	public void setTitleName(String mTitleName) {
		this.mTitleName = mTitleName;
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		L.i(getTvTag(), "setUserVisibleHint()-->"+isVisibleToUser);
		if(!mClipChildren){
			View _myView = getView();
			if(_myView!=null){
				if(isVisibleToUser){
					if(ViewGroup.class.isInstance(_myView)){
						((ViewGroup)_myView).setClipChildren(false);
					}
					_myView.bringToFront();
				}else{
					if(ViewGroup.class.isInstance(_myView)){
						((ViewGroup)_myView).setClipChildren(true);
					}
				}
			}	
		}
		
	}
	public boolean isClipChildren() {
		return mClipChildren;
	}
	public void setClipChildren(boolean mClipChildren) {
		this.mClipChildren = mClipChildren;
	}

}
