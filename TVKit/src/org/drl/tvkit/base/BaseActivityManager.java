package org.drl.tvkit.base;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;

public final class BaseActivityManager {
	
	public static final BaseActivityManager getInstance()
	{
		if(mInstance==null){
			mInstance = new BaseActivityManager();
		}
		return mInstance;
	}
	private static BaseActivityManager mInstance = null;
	
	private Map<String,Activity> cacheActivitys;
	
	private BaseActivityManager() {
		cacheActivitys = new HashMap<String, Activity>();
	}
	public void  onRegeterActivity(Activity _aty) {
		if(_aty!=null){
		cacheActivitys.put(_aty.getClass().getSimpleName(), _aty);
		}
		
	}
	public void  onUnRegeterActivity(Activity _aty) {
		if(_aty!=null){
			cacheActivitys.remove(_aty.getClass().getSimpleName());
		}
	}
	public void onfinishAllActivity()
	{
		Collection<Activity> atys =  cacheActivitys.values();
		for(Activity _aty:atys){
			try{
			_aty.finish();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

}
