package org.drl.tvkit.ui.fragment;

import org.drl.tvkit.R;
import org.drl.tvkit.base.fragment.TVFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

/**
 * @author Darry Ring
 * 
 */
public class HomeFragment extends TVFragment implements OnClickListener {

	View rootView;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		rootView = getView();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View _view = inflater.inflate(R.layout.root_ft_home_r2,
				container, false);
		return _view;
	}
	@Override
	public void onResume() {
		super.onResume();

	}
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
	}
	@Override
	public void onPause() {
		super.onPause();
	}
	@Override
	public void onClick(View v) {
		
	}

	

}
