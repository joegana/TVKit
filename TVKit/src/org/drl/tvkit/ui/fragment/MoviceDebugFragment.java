package org.drl.tvkit.ui.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.drl.tvkit.R;
import org.drl.tvkit.base.fragment.TVFragment;
import org.drl.tvkit.util.L;
import org.drl.tvkit.util.UrlFormatUtil;
import org.drl.tvkit.widget.MovieView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

/**
 * 
 * MovieView--->onDraw绘图调试
 * @author Darry Ring
 * 
 */
public class MoviceDebugFragment extends TVFragment implements OnClickListener {

	View rootView;
	RelativeLayout mLayout;
	Map<String,Drawable> cacheIcons = new HashMap<String,Drawable>();
	List<ResolveInfo> apks = new ArrayList<ResolveInfo>();
	List<Integer> imageIds = new ArrayList<Integer>();
	List<MovieView> mListMovies = new ArrayList<MovieView>();
	private DisplayImageOptions options = null;

	private void buildImageOptionis() {
		if (options == null) {
			options = new DisplayImageOptions.Builder()
					// .resetViewBeforeLoading(true)
					.cacheOnDisk(false).cacheInMemory(true)
					.displayer(new RoundedBitmapDisplayer(10))
					.bitmapConfig(Bitmap.Config.ARGB_8888).build();
		}
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		PackageManager pm = getActivity().getPackageManager();
		List<ResolveInfo> apps =pm.queryIntentActivities(mainIntent, 0);
		if (apps == null) {
		    return;
		}
		 
		for (ResolveInfo info : apps) {
		    String   _pname = info.activityInfo.applicationInfo.packageName; 
		    Drawable _icon =  info.loadIcon(pm);
		    String   _name = info.loadLabel(pm).toString();
		    cacheIcons.put(_pname, _icon);
		    L.i("SoftwareFragment", "packetName-->"+_pname);
		    L.i("SoftwareFragment", "name-->"+_name);
		    apks.add(info);
		}
		int count = apks.size()/4+1;
		for(int i=0;i<count;i++){
			imageIds.add(R.drawable.ic_style_color_1);
			imageIds.add(R.drawable.ic_style_color_2);
			imageIds.add(R.drawable.ic_style_color_3);
			imageIds.add(R.drawable.ic_style_color_4);
			imageIds.add(R.drawable.ic_style_color_5);
			imageIds.add(R.drawable.ic_style_color_6);
			imageIds.add(R.drawable.ic_style_color_7);
			imageIds.add(R.drawable.ic_style_color_8);
			imageIds.add(R.drawable.ic_style_color_9);
			imageIds.add(R.drawable.ic_style_color_10);
		}
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		rootView = getView();
		buildImageOptionis();
		initView();
	}
	
	void initView() {
		mLayout = (RelativeLayout) rootView.findViewById(R.id.panel_layout);
		mListMovies.clear();
		addViewPanel(mLayout, apks.size(), mListMovies);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View _view = inflater.inflate(R.layout.root_ft_home_r1,
				container, false);
		mLayout = (RelativeLayout) _view.findViewById(R.id.panel_layout);
		return _view;
	}
	void initViewForData() {
		for (int i = 0; i < mListMovies.size(); i++) {
			MovieView _mv = mListMovies.get(i);
			if (_mv != null) {
				if(i<imageIds.size()){
					_mv.setDebugDraw(Boolean.TRUE);
					_mv.setImageResource(imageIds.get(i));
					_mv.setImageFile(UrlFormatUtil.formatUrlDrawable(imageIds.get(i)));
				}
			}
		}
	}

	@SuppressLint("InflateParams")
	void addViewPanel(RelativeLayout layout, int size, List<MovieView> mListView) {
		LayoutInflater _in = LayoutInflater.from(getActivity());
		int index = 0;
		for (int i = 0; i < size; i++) {
			RelativeLayout.LayoutParams params = new LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// ---------------------------------------------------------
			View _view = _in.inflate(R.layout.item_apk_movice_r1, null);
			_view.setId((int) System.currentTimeMillis()+i);
			if (i == 0) {
				params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
				params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
				params.leftMargin = 80;
			} else{
				params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
				params.addRule(RelativeLayout.RIGHT_OF, layout
						.getChildAt(i - 1).getId());
				params.leftMargin = 10;
			}
			MovieView _mv = (MovieView) _view.findViewById(R.id.ify_item_movie_01);
			layout.addView(_view, params);
			//
			mListView.add(_mv);
			index = i;
		}
		RelativeLayout.LayoutParams params = new LayoutParams(100, LayoutParams.WRAP_CONTENT);
		View _view = new View(getActivity());
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.RIGHT_OF, layout.getChildAt(index).getId());
		layout.addView(_view, params);
		_view.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onResume() {
		super.onResume();
		initViewForData();
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onClick(View v) {

	}

}
