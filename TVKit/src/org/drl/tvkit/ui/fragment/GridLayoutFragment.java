package org.drl.tvkit.ui.fragment;

import org.drl.tvkit.R;
import org.drl.tvkit.base.fragment.TVFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;

/**
 * @author Darry Ring
 * 
 */
public class GridLayoutFragment extends TVFragment implements OnClickListener {

	View rootView;
	GridLayout mGridLayout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		rootView = getView();
		mGridLayout = (GridLayout) rootView.findViewById(R.id.gridlayout);
		mGridLayout.setColumnCount(20/2);
		for(int i=0;i<20;i++){
			Button _btn = new Button(getActivity());
			_btn.setText("Button");
			mGridLayout.addView(_btn);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View _view = inflater.inflate(R.layout.root_ft_gridlayout_r1,
				container, false);
		return _view;
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onClick(View v) {

	}

}
