package org.drl.tvkit.ui.fragment;

import java.util.ArrayList;
import java.util.List;

import org.drl.tvkit.R;
import org.drl.tvkit.base.fragment.TVFragment;
import org.drl.tvkit.ui.adapter.GridAdapter;
import org.drl.tvkit.widget.TVGridView;
import org.drl.tvkit.widget.TVListView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Toast;

/**
 * @author Darry Ring
 * 
 */
public class ListFragment extends TVFragment implements OnClickListener {

	View rootView;
	TVListView mGridView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		List<String> mLm = new ArrayList<String>();
		for (int i = 0; i < 16; i++) {
			mLm.add(String.valueOf(i));
		}
		mGridAdapter = new GridAdapter(getActivity(), mLm, null);
	}

	GridAdapter mGridAdapter;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		rootView = getView();
		mGridView = (TVListView) rootView.findViewById(R.id.gv_1);
		mGridView.setAdapter(mGridAdapter);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View _view = inflater.inflate(R.layout.root_ft_list_r1,
				container, false);
		return _view;
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onClick(View v) {

	}

}
