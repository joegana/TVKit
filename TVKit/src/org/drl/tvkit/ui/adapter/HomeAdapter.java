package org.drl.tvkit.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import org.drl.tvkit.base.fragment.TVFragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class HomeAdapter extends FragmentPagerAdapter {

	List<TVFragment> mFragments = new ArrayList<TVFragment>();

	public HomeAdapter(FragmentManager fm) {
		super(fm);
	}

	public void clear() {
		if (mFragments != null) {
			mFragments.clear();
		}
	}

	public void addTVFragment(TVFragment _fragment) {
		mFragments.add(_fragment);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		if (mFragments.size() > 0) {
			return mFragments.get(position).getTitleName();
		}
		return "";
	}

	@Override
	public int getCount() {
		return mFragments.size();
	}

	@Override
	public TVFragment getItem(int position) {
		return mFragments.get(position);
	}

}