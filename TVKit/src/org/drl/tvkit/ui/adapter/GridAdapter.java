package org.drl.tvkit.ui.adapter;

import java.util.List;

import org.drl.tvkit.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class GridAdapter extends ArrayAdapter<String> {
	LayoutInflater in = null;
	OnFocusChangeListener mOnFocusChangeListener;
	public GridAdapter(Context context, List<String> objects,OnFocusChangeListener listener) {
		super(context, 0, objects);
		in = LayoutInflater.from(getContext());
		mOnFocusChangeListener = listener;
	}
	@Override
	public View getView(int position, View mVm, ViewGroup parent) {
		if(mVm==null){
			mVm = in.inflate(R.layout.grid_item, null);
//			mVm.setFocusable(true);
//			mVm.setClickable(true);
//			mVm.setFocusableInTouchMode(Boolean.TRUE);
//			mVm.setOnFocusChangeListener(mOnFocusChangeListener);
		}
		DataTag _tag = new DataTag();
		_tag.position = position;
		mVm.setTag(_tag);
		return mVm;
	}
	
	public static class DataTag{
		public int position;
	}

}
