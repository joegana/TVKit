package org.drl.tvkit.ui;

import org.drl.tvkit.R;
import org.drl.tvkit.base.BaseFragmentFragment;
import org.drl.tvkit.base.actionbar.BaseActionPanel;
import org.drl.tvkit.base.bottombar.BaseBottomPanel;
import org.drl.tvkit.base.fragment.TVFragment;
import org.drl.tvkit.ui.adapter.HomeAdapter;
import org.drl.tvkit.ui.fragment.GridFragment;
import org.drl.tvkit.ui.fragment.GridLayoutFragment;
import org.drl.tvkit.ui.fragment.ListFragment;
import org.drl.tvkit.ui.fragment.MoviceDebugFragment;
import org.drl.tvkit.ui.fragment.SoftwareFragment;
import org.drl.tvkit.widget.BasePagerTabStrip;
import org.drl.tvkit.widget.VooleViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * @author Darry Ring
 * 
 */
public class HomeActivity extends BaseFragmentFragment {

	BaseActionPanel mBaseActionPanel;
	BaseBottomPanel mBaseBottomPanel;
	RelativeLayout mActionPanelLayout;
	RelativeLayout mBotoomPanelLayout;
	VooleViewPager pager;
	HomeAdapter adapter;
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.root_home_r1);
		setupPanel();
		setupActionPanel();
		setupBottomPanel();
		findViews();
		setupFragment();
	}
	protected void findViews()
	{
		pager = (VooleViewPager) findViewById(R.id.pager);
	}
	@Override
	protected void onResume() {
		super.onResume();

	}
	protected void setupPanel(){
		mActionPanelLayout = (RelativeLayout) findViewById(R.id.actionbar_layout);
		mBotoomPanelLayout = (RelativeLayout) findViewById(R.id.view_bottom);
	}
	protected void setupActionPanel()
	{
		mBaseActionPanel = BaseActionPanel.getInstance("org.drl.tvkit.base.actionbar.TvKitActionPanel");
		mBaseActionPanel.onCreate(this);
		final View _view   = mBaseActionPanel.onCreateView(getLayoutInflater());
		if(_view!=null){
			mActionPanelLayout.addView(_view);
		}
		
	}
	protected void setupBottomPanel(){
		mBaseBottomPanel = BaseBottomPanel.getInstance("org.drl.tvkit.base.bottombar.TvKitBottomPanel");
		mBaseActionPanel.onCreate(this);
		final View _view   = mBaseBottomPanel.onCreateView(getLayoutInflater());
		if(_view!=null){
			mBotoomPanelLayout.addView(_view);
		}
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
	}
	protected void setupFragment()
	{
		TVFragment _t1 = new GridFragment();
		TVFragment _t2 = new SoftwareFragment();
		TVFragment _t3 = new MoviceDebugFragment();
		TVFragment _t4 = new ListFragment();
		TVFragment _t5 = new GridLayoutFragment();
		_t1.setTitleName("主页");
		_t2.setTitleName("软件");
		_t3.setTitleName("绘图调试");
		_t4.setTitleName("ListFragment");
		_t5.setTitleName("GridLayout");
		adapter = new HomeAdapter(getSupportFragmentManager());
		adapter.addTVFragment(_t2);
		adapter.addTVFragment(_t3);
		adapter.addTVFragment(_t1);
		adapter.addTVFragment(_t4);
		adapter.addTVFragment(_t5);
		adapter.notifyDataSetChanged();
		pager.setAdapter(adapter);
		View _tabbar = findViewById(R.id.tabs);
		if (_tabbar != null) {
			if (_tabbar instanceof BasePagerTabStrip) {
				BasePagerTabStrip _tabStrip = (BasePagerTabStrip) _tabbar;
				_tabStrip.setViewPager(pager);
			}
		}
		pager.setOffscreenPageLimit(0);
		pager.requestLayout();
		pager.requestFocus();
	}
	

}
