package org.drl.tvkit.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;


/**
 * @author dai.rui.lin
 *
 */
public final class FileUtils {
	
	 public static void writeFile(String data, File file) throws IOException {
	        writeFile(data.getBytes(Charset.forName("UTF-8")), file);
	    }

	    public static void writeFile(byte[] data, File file) throws IOException {
	        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file, false));
	        bos.write(data);
	        bos.close();
	    }

	    public static String readFileAsString(File file) throws IOException {
	        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        IOUtils.copy(bis, bos);
	        byte[] contents = bos.toByteArray();
	        bis.close();
	        bos.close();
	        return new String(contents, Charset.forName("UTF-8"));
	    }
	
	
	public final static String getExtFileName(String url)
	{
		String name = url.substring(url.lastIndexOf('.'));
		
		return name;
	}
	
	public final static boolean hasFile(File path,String fileName){
		File mFile = new File(path, fileName);
		return mFile.exists();
	}
	
	/**
	 * @param path
	 * @param fileName
	 * @return
	 */
	public final static String readFileString(File path,String fileName){
		File mFile = new File(path, fileName);
		String mString = null;
		if(!mFile.exists()){
			return null;
		}
		try {
			FileInputStream fr= new FileInputStream(mFile);
			byte bytes[] = readFileInputStream(fr);
			mString = new String(bytes);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return mString;
	}

	/**
	 * @param path
	 * @param fileName
	 * @param buf
	 */
	public final static void saveFile(File path, String fileName, String buf) {
		if(path==null || fileName==null || buf==null){
//			throw new NullPointerException("FileUtils saveFile args NullPointerException");
			return ;
		}
		File mFile = new File(path, fileName);
		if (mFile.exists()) {
			mFile.delete();
		}
		try {
			mFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			FileOutputStream out = new FileOutputStream(mFile);
			out.write(buf.getBytes());
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	/**
	 * @param path
	 * @param fileName
	 * @param buf
	 */
	public final static File saveFile(File path, String fileName, byte[] buf) {
		if(path==null || fileName==null || buf==null){
			return null;
		}
		File mFile = new File(path, fileName);
		if (mFile.exists()) {
			mFile.delete();
		}
		try {
			mFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			FileOutputStream out = new FileOutputStream(mFile);
			out.write(buf);
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return mFile;
		
	}
	
	final public static byte[] readStream(InputStream inStream)
			throws Exception {
		ByteArrayOutputStream outstream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = -1;
		while ((len = inStream.read(buffer)) != -1) {
			outstream.write(buffer, 0, len);
		}
		outstream.close();
		inStream.close();
		
		return outstream.toByteArray();
	}
	final public static byte[] readFileInputStream(FileInputStream inStream)
			throws Exception {
		ByteArrayOutputStream outstream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = -1;
		while ((len = inStream.read(buffer)) != -1) {
			outstream.write(buffer, 0, len);
		}
		outstream.close();
		inStream.close();

		return outstream.toByteArray();
	}

}
