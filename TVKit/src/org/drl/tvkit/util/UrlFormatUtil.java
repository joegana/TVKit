package org.drl.tvkit.util;


public final class UrlFormatUtil {
	
	public final static String formatUrlDrawable(int resid){
		StringBuffer _buf = new StringBuffer("drawable://");
		_buf.append(resid);
		return _buf.toString();
	}
	public final static String formatUrlFile(String _file){
		StringBuffer _buf = new StringBuffer("file://");
		_buf.append(_file);
//		try{
//		FileRecordTable _table = FileRecordTable.getInstance();
//		_table.writeItemLong(_file, System.currentTimeMillis());
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}
		return _buf.toString();
	}
	public final static String formatUrlAssets(String _file){
		StringBuffer _buf = new StringBuffer("assets://");
		_buf.append(_file);
		return _buf.toString();
	}

}
