package org.drl.tvkit.util;

import android.util.Log;

public final class L {

	public static void i(String tag,String msg){
		Log.i(tag, msg);
	}
	public static void d(String tag,String msg){
		Log.d(tag, msg);
	}
	public static void e(String tag,String msg){
		Log.e(tag, msg);
	}
}
