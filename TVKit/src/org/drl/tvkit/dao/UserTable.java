package org.drl.tvkit.dao;

import java.util.HashMap;
import java.util.Map;

import org.drl.tvkit.Config;
import org.drl.tvkit.util.L;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;


/**
 * @author dai.rui.lin
 *
 */
public class UserTable extends AbstractTable {
	public static final class Fields implements BaseColumns{
		
		public static final String TAB_NAME         = "UserTable"  ;
		//
		public static final String DATA_KEY         = "_DATA_KEY"      ;
		public static final String DATA_BODY        = "_DATA_BODY"     ;
		
		public static final String COLUMNS[] = {_ID,DATA_KEY,DATA_BODY};
	}
	
	static int getId(Cursor cursor) {
		return cursor.getInt(cursor.getColumnIndex(Fields._ID));
	}
	static String getKey(Cursor cursor) {
		return cursor.getString(cursor.getColumnIndex(Fields.DATA_KEY));
	}
	static String getBody(Cursor cursor) {
		return cursor.getString(cursor.getColumnIndex(Fields.DATA_BODY));
	}
	
	
	private static UserTable instance;
	public static UserTable getInstance() {
		if(instance==null){
			instance = new UserTable();
		}
		return instance;
	}
	private UserTable() {
	}

	@Override
	public void create(SQLiteDatabase db) {
		String sql;
		sql = "CREATE TABLE " + Fields.TAB_NAME + " (" 
				+ Fields._ID+ " INTEGER PRIMARY KEY," 
				+ Fields.DATA_KEY + " TEXT,"
				+ Fields.DATA_BODY +  " TEXT);";
		DataBaseManager.execSQL(db, sql);
		
	}
	
	public int writeItem(String key,String body){
		L.i(Config.APP_LOG_TAG, "UserTable-->writeItem-->key="+key+" ,body="+body);
		ContentValues cv = new ContentValues();
		cv.put(Fields.DATA_KEY, key);
		cv.put(Fields.DATA_BODY, body);
		
		SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
		int $id = hasItem(key);
		if($id>-1){
			int $rid = db.update(getTableName(), cv, Fields._ID+"=?", new String[]{String.valueOf($id)});
			return $rid;
		}else{
			long id = db.insert(getTableName(), null, cv);
			return (int) id;
		}
	}
	public int deleteItem(String key){
		L.i(Config.APP_LOG_TAG, "UserTable-->deleteItem-->key="+key);
		SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
		int _id = db.delete(getTableName(), Fields.DATA_KEY+"=?", new String[]{key});
		return _id;
	}
	public int hasItem(String key){
		SQLiteDatabase db = DataBaseManager.getInstance().getReadableDatabase();
		Cursor c = db.query(getTableName(), getProjection(), Fields.DATA_KEY+"=?", new String[]{key}, null, null, null);
	    if(hasData(c)){
	    	c.moveToFirst();
	    	int _id = c.getInt(c.getColumnIndex(Fields._ID));
	    	closeCurosr(c);
	    	return _id;
	    }else{
	    	closeCurosr(c);
	    	return -1;
	    }
	}
	public boolean hasKeyValue(String key){
		SQLiteDatabase db = DataBaseManager.getInstance().getReadableDatabase();
		Cursor c = db.query(getTableName(), getProjection(), Fields.DATA_KEY+"=?", new String[]{key}, null, null, null);
		if(hasData(c)){
			c.moveToFirst();
//			int _id = c.getInt(c.getColumnIndex(Fields._ID));
			closeCurosr(c);
			return Boolean.TRUE;
		}else{
			closeCurosr(c);
			return Boolean.FALSE;
		}
	}
	
	public Map<String,String> readMap()
	{
		Map<String,String> mRm = new HashMap<String,String>();
		Cursor  c = list();
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			String $key = getKey(c);
			String $body = getBody(c);
			mRm.put($key, $body);
		}
		closeCurosr(c);
		return mRm;
	}
	public String readItem(String _key,String _defalut)
	{
		String _result = _defalut;
		Cursor  c = list(Fields.DATA_KEY+"=?",new String[]{_key});
		if(c!=null && c.getCount()>0){
			c.moveToFirst();
			String $body = getBody(c);
			_result = $body;
		}
		closeCurosr(c);
		return _result;
	}

	@Override
	protected String getTableName() {
		return Fields.TAB_NAME;
	}

	@Override
	protected String[] getProjection() {
		return Fields.COLUMNS;
	}
}
