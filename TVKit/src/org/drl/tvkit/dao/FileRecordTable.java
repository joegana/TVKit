package org.drl.tvkit.dao;

import java.io.File;

import org.drl.tvkit.Config;
import org.drl.tvkit.task.TaskSub;
import org.drl.tvkit.util.L;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;


/**
 * @author dai.rui.lin
 *
 */
public class FileRecordTable extends AbstractTable implements TaskSub {
	public static final class Fields implements BaseColumns{
		
		public static final String TAB_NAME         = "FileRecordTable"  ;
		//
		public static final String DATA_KEY         = "_DATA_KEY"      ;
		public static final String DATA_BODY        = "_DATA_TIME"     ;
		
		public static final String COLUMNS[] = {_ID,DATA_KEY,DATA_BODY};
	}
	
	static int getId(Cursor cursor) {
		return cursor.getInt(cursor.getColumnIndex(Fields._ID));
	}
	static String getKey(Cursor cursor) {
		return cursor.getString(cursor.getColumnIndex(Fields.DATA_KEY));
	}
	static Long getTime(Cursor cursor) {
		return cursor.getLong(cursor.getColumnIndex(Fields.DATA_BODY));
	}
	
	
	private static FileRecordTable instance;
	public static FileRecordTable getInstance() {
		if(instance==null){
			instance = new FileRecordTable();
		}
		return instance;
	}
	private FileRecordTable() {
	}

	@Override
	public void create(SQLiteDatabase db) {
		String sql;
		sql = "CREATE TABLE " + Fields.TAB_NAME + " (" 
				+ Fields._ID+ " INTEGER PRIMARY KEY," 
				+ Fields.DATA_KEY + " TEXT,"
				+ Fields.DATA_BODY +  " TEXT);";
		DataBaseManager.execSQL(db, sql);
		
	}
	
	public int writeItemLong(String key,long body){
		L.i(Config.APP_LOG_TAG, "KeyValueTable-->writeItem-->key="+key+" ,body="+body);
		ContentValues cv = new ContentValues();
		cv.put(Fields.DATA_KEY, key);
		cv.put(Fields.DATA_BODY, String.valueOf(body));
		
		SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
		int $id = hasItem(key);
		if($id>-1){
			int $rid = db.update(getTableName(), cv, Fields._ID+"=?", new String[]{String.valueOf($id)});
			return $rid;
		}else{
			long id = db.insert(getTableName(), null, cv);
			return (int) id;
		}
	}
	@Override
	public void runTaskSub() {
		final long _timeOut = 1296000000;
		Cursor  c = list();
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			String $mFile = getKey(c);
			long $time = getTime(c);
			long curTime = (System.currentTimeMillis()-$time);
			if(curTime>_timeOut){
				//文件15天未使用,需要清理
				File mFm = new File($mFile);
				if(mFm.exists()){
					mFm.delete();
				}
			}
		}
		closeCurosr(c);
	}
	public int deleteItem(String key){
		L.i(Config.APP_LOG_TAG, "KeyValueTable-->deleteItem-->key="+key);
		SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
		int _id = db.delete(getTableName(), Fields.DATA_KEY+"=?", new String[]{key});
		return _id;
	}
	public int hasItem(String key){
		SQLiteDatabase db = DataBaseManager.getInstance().getReadableDatabase();
		Cursor c = db.query(getTableName(), getProjection(), Fields.DATA_KEY+"=?", new String[]{key}, null, null, null);
	    if(hasData(c)){
	    	c.moveToFirst();
	    	int _id = c.getInt(c.getColumnIndex(Fields._ID));
	    	closeCurosr(c);
	    	return _id;
	    }else{
	    	closeCurosr(c);
	    	return -1;
	    }
	}
	public boolean hasKeyValue(String key){
		SQLiteDatabase db = DataBaseManager.getInstance().getReadableDatabase();
		Cursor c = db.query(getTableName(), getProjection(), Fields.DATA_KEY+"=?", new String[]{key}, null, null, null);
		if(hasData(c)){
			c.moveToFirst();
//			int _id = c.getInt(c.getColumnIndex(Fields._ID));
			closeCurosr(c);
			return Boolean.TRUE;
		}else{
			closeCurosr(c);
			return Boolean.FALSE;
		}
	}
	
	public long readItem(String _key,long _defalut)
	{
		Long _result = _defalut;
		Cursor  c = list(Fields.DATA_KEY+"=?",new String[]{_key});
		if(c!=null && c.getCount()>0){
			c.moveToFirst();
			long $body = getTime(c);
			_result = $body;
		}
		closeCurosr(c);
		return _result;
	}
	public long readItemLong(String _key,int _defalut)
	{
		long _result = _defalut;
		Cursor  c = list(Fields.DATA_KEY+"=?",new String[]{_key});
		if(c!=null && c.getCount()>0){
			c.moveToFirst();
			_result = getTime(c);
		}
		closeCurosr(c);
		return _result;
	}
	@Override
	protected String getTableName() {
		return Fields.TAB_NAME;
	}

	@Override
	protected String[] getProjection() {
		return Fields.COLUMNS;
	}
}
