package org.drl.tvkit.widget;

import java.io.File;
import java.io.FileOutputStream;

import org.drl.tvkit.util.L;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader.TileMode;
import android.view.View;
import android.view.ViewGroup;

public class ShadowUtil {

	
	public static final String  outBitmapShadowFile(Bitmap srcBm,View srcView, ShadowView mShadowView,File _out) {
		if(_out.exists()){
			return _out.toString();
		}
		L.i("ShadowUtil", "-->outBitmapShadowFile");
		Matrix matrix = new Matrix();
		matrix.postScale(1, -1);
		Bitmap scrbm = srcBm;
		if(scrbm==null){
			scrbm = Bitmap.createBitmap(srcView.getWidth(), srcView.getHeight(), Config.ARGB_8888);
			Canvas _c = new Canvas(scrbm);
			srcView.draw(_c);
		}
		if (scrbm != null) {
			Bitmap _buf = Bitmap.createBitmap(mShadowView.getWidth(),
					mShadowView.getHeight(), Bitmap.Config.ARGB_8888);
			Canvas _canvs = new Canvas(_buf);
			_canvs.setDrawFilter(new PaintFlagsDrawFilter(0,
					Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG));
			int y = srcView.getHeight() - mShadowView.getHeight();
			if (y < 0) {
				y = 0;
			}
			Bitmap bm = Bitmap.createBitmap(scrbm, 0, y,
					mShadowView.getWidth(), mShadowView.getHeight(), matrix,false);
			_canvs.drawBitmap(bm, 0, 0, null);
			Paint paint = new Paint();
			scrbm.recycle();
			scrbm = null;
//			LinearGradient shader = new LinearGradient(0, 0, 0,
//					srcView.getHeight(), 0x40888888, 0x00000000, TileMode.CLAMP);
			LinearGradient shader = new LinearGradient(0, 0, 0,
					srcView.getHeight(), Color.parseColor("#4D888888"), Color.parseColor("#00000000"), TileMode.REPEAT);
			paint.setShader(shader);
			paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
			_canvs.drawRect(0, 0, mShadowView.getWidth(),
					mShadowView.getHeight(), paint);
			_canvs.save();
			
			bm.recycle();
			bm = null;
			try {
				_out.createNewFile();
				FileOutputStream out = new FileOutputStream(_out);
				_buf.compress(Bitmap.CompressFormat.PNG, 100, out);
				out.flush();
				out.close();
				_buf.recycle();
				_buf = null;
				L.i("ShadowUtil", "saved->"+_out.toString());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return _out.toString();
		}
		return _out.toString();
	}
	
	
	public static final String  outBitmapShadowLayoutFile(ViewGroup group,Bitmap srcBm,View srcView, ShadowView mShadowView,File _out) {
		if(_out.exists()){
			return _out.toString();
		}
		L.i("ShadowUtil", "-->outBitmapShadowFile");
		Matrix matrix = new Matrix();
		matrix.postScale(1, -1);
		Bitmap scrbm = srcBm;
		if(group!=null){
			group.setDrawingCacheEnabled(false);
		}
		if(scrbm==null){
			scrbm = Bitmap.createBitmap(srcView.getWidth(), srcView.getHeight(), Config.ARGB_8888);
			Canvas _c = new Canvas(scrbm);
			group.draw(_c);
		}
		if (scrbm != null) {
			Bitmap _buf = Bitmap.createBitmap(mShadowView.getWidth(),
					mShadowView.getHeight(), Bitmap.Config.ARGB_8888);
			Canvas _canvs = new Canvas(_buf);
			_canvs.setDrawFilter(new PaintFlagsDrawFilter(0,
					Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG));
			int y = srcView.getHeight() - mShadowView.getHeight();
			if (y < 0) {
				y = 0;
			}
			Bitmap bm = Bitmap.createBitmap(scrbm, 0, y,
					mShadowView.getWidth(), mShadowView.getHeight(), matrix,false);
			_canvs.drawBitmap(bm, 0, 0, null);
			Paint paint = new Paint();
			scrbm.recycle();
			scrbm = null;
			LinearGradient shader = new LinearGradient(0, 0, 0,
					srcView.getHeight(), Color.parseColor("#4D888888"), Color.parseColor("#00000000"), TileMode.REPEAT);
			paint.setShader(shader);
			paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
			_canvs.drawRect(0, 0, mShadowView.getWidth(),
					mShadowView.getHeight(), paint);
			_canvs.save();
			
			bm.recycle();
			bm = null;
			try {
				_out.createNewFile();
				FileOutputStream out = new FileOutputStream(_out);
				_buf.compress(Bitmap.CompressFormat.PNG, 100, out);
				out.flush();
				out.close();
				_buf.recycle();
				_buf = null;
				L.i("ShadowUtil", "saved->"+_out.toString());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return _out.toString();
		}
		return _out.toString();
	}

}
