package org.drl.tvkit.widget;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.drl.tvkit.KitApplication;
import org.drl.tvkit.util.L;
import org.drl.tvkit.util.UrlFormatUtil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

@SuppressLint("HandlerLeak")
public class ShadowManagerImpl extends ShadowManager implements Runnable {
	private static final int MAX_RELOAD_COUNT = 30;
	public static final class Data {
		public MovieView mSrcView;
		public ShadowView mShadowView;
		public String mShadowFile;
		public int mCount = 0;
	}
	private final BlockingQueue<Data> mQueue;
	public void quit() {
		try {
			mQuit = true;
			mThread.interrupt();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		L.d(TAG, "quit");
	}
	private volatile boolean mQuit = false;
	String TAG = "ShadowManagerImpl";
	private File mFile;
	private Thread mThread;
	private ImageLoader mImageLoader;
	public ShadowManagerImpl(Context _ctx) {
		super(_ctx);
		mThread = new Thread(this);
		if(KitApplication.getInstance()!=null){
			mImageLoader = KitApplication.getInstance().getImageLoader();
		}
		mQueue = new LinkedBlockingQueue<Data>();
		mFile = new File("");
		try{
		File _f = getContext().getCacheDir();
		mFile = new File(_f.toString()+File.separator+"shadows");
			if(!mFile.exists()){
				mFile.mkdirs();
			}
		}catch(Exception ex){
			
		}
		mThread.start();
	}
	@Override
	public void removeShadowCacheFile(MovieView _movieView) {
		File _del = new File(mFile,_movieView.getId()+".png");
		if(_del.exists()){
			_del.delete();
			L.i(TAG,"-->del file-->"+_del.toString());
		}
	}
	@Override
	public synchronized void run() {
		while (true) {
			Data _data;
			try {
				_data = mQueue.take();
			} catch (InterruptedException e) {
				if (mQuit) {
					return;
				}
				continue;
			}
			if(_data!=null){
				_data.mCount++;
				loadShadowFile(_data);
			}
		}
	}
	private synchronized void loadShadowFile(final Data _data){
		if (_data != null) {
			File _dir = new File(mFile.toString());
			if (!_dir.exists()) {
				_dir.mkdirs();
			}
			String _name = _data.mSrcView.getShadowFileName();
			final File _file = new File(_dir,_name);
			Log.i("MovieView", _file.toString());
			MemoryCacheUtils.removeFromCache(UrlFormatUtil
					.formatUrlFile(_file.toString()),
					mImageLoader.getMemoryCache());
			if (!_file.exists()) {
				int _width = _data.mSrcView.getWidth();
				int _height = _data.mSrcView.getHeight();
//				------------------------------------------
				int _swidth = _data.mShadowView.getWidth();
				int _sheight= _data.mShadowView.getHeight();
				Drawable _drawable = _data.mSrcView.getDrawable();
				if(_width>0&&_height>0&&_swidth>0&&_sheight>0&&_drawable!=null){
					View _view = (View) _data.mSrcView.getParent();
					if(_view==null){
						ShadowUtil.outBitmapShadowFile(
								_data.mSrcView.getDrawingCache(), _data.mSrcView,
								_data.mShadowView, _file);
					}else{
						if(ViewGroup.class.isInstance(_view)){
							ViewGroup _group = (ViewGroup) _view;
							if(_group.getChildCount()>2){
								ShadowUtil.outBitmapShadowLayoutFile((ViewGroup)_view,
										null, _data.mSrcView,
										_data.mShadowView, _file);
							}else{
								ShadowUtil.outBitmapShadowFile(
										_data.mSrcView.getDrawingCache(), _data.mSrcView,
										_data.mShadowView, _file);
							}
						}
					}
				}else if(_width==0||_height==0||_swidth==0||_sheight==0||_drawable==null){
					return;
				}else{
					if(isReloadAdd()){
						Message _waitMsg = mHandler.obtainMessage(ADD_SHADOW_TASK);
						_waitMsg.obj = _data;
						mHandler.sendMessageDelayed(_waitMsg, 3000);
						return;
					}else{
						Message _waitMsg = mHandler.obtainMessage(ADD_SHADOW_MAX_TASK);
						_waitMsg.obj = _data;
						mHandler.sendMessageDelayed(_waitMsg, 2000);
						return;
					}
				}
			}
			_data.mShadowFile = _file.toString();
			Message _msg = mHandler.obtainMessage(LOAD_SHADOW_FILE);
			_msg.obj = _data;
			mHandler.sendMessage(_msg);
		}
	}
	private final int LOAD_SHADOW_FILE =2;
	private final int ADD_SHADOW_TASK = 3;
	private final int ADD_SHADOW_MAX_TASK = 4;
	android.os.Handler mHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			switch(msg.what){
			case LOAD_SHADOW_FILE:
				if(msg.obj!=null){
					Data _data = (Data) msg.obj;
					_data.mSrcView.loadShadowAsyn(_data.mShadowFile);
				}
				break;
			case ADD_SHADOW_TASK:
				if(msg.obj!=null){
					Data _data = (Data) msg.obj;
					mQueue.add(_data);
				}
				break;
			case ADD_SHADOW_MAX_TASK:
				if(msg.obj!=null){
					Data _data = (Data) msg.obj;
					if(_data.mCount<MAX_RELOAD_COUNT){
						mQueue.add(_data);
					}
				}
				break;
			}
		}
	};
	@Override
	public void loadShadowAsyn(MovieView _movieView, ShadowView _shadow) {
		Data _data = new Data();
		_data.mSrcView = _movieView;
		_data.mShadowView = _shadow;
		mQueue.add(_data);
	}
}
