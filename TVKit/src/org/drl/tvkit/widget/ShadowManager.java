package org.drl.tvkit.widget;

import android.content.Context;



public abstract class ShadowManager {

	private static ShadowManager mShadowManager = null;
	private Context mContext;
	private boolean isReloadAdd;
	public static final ShadowManager getInstance(Context _ctx) {
		if (mShadowManager == null) {
			mShadowManager = new ShadowManagerImpl(_ctx);
		}
		return mShadowManager;
	}
	public ShadowManager(Context _ctx) {
		this.mContext = _ctx;
	}
	public Context getContext()
	{
		return mContext;
	}
	/**
	 * 删除倒影临时文件
	 * @param _movieView
	 */
	public abstract void removeShadowCacheFile(MovieView _movieView);
	/**
	 * 根据海报组件动态生成倒影图片保存本地路径，然后在刷新view
	 * @param _movieView
	 * @param _shadow
	 */
	public abstract void loadShadowAsyn(MovieView _movieView, ShadowView _shadow);

	public boolean isReloadAdd() {
		return isReloadAdd;
	}
	public void setReloadAdd(boolean isReloadAdd) {
		this.isReloadAdd = isReloadAdd;
	}
}
