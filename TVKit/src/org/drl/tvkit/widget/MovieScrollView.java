package org.drl.tvkit.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.HorizontalScrollView;

/**
 * 横向滑动器
 * @author Darry Ring
 *
 */
public class MovieScrollView extends HorizontalScrollView {
	String TAG = "MovieScrollView";
	 int screenWidth;
	public MovieScrollView(Context context) {
		super(context);
		screenWidth = getResources().getDisplayMetrics().widthPixels;
	}

	public MovieScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		screenWidth = getResources().getDisplayMetrics().widthPixels;
	}

	public MovieScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		screenWidth = getResources().getDisplayMetrics().widthPixels;
	}
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
//		ViewParent _parent = getParent();
//		//设置可超出ViewGroup范围绘制Canvas
//		if(_parent!=null){
//			if(ViewGroup.class.isInstance(_parent)){
//				ViewGroup _group = (ViewGroup) _parent;
//				_group.setClipChildren(Boolean.FALSE);
//				_group.bringToFront();
//			}
//		}
	}
	private boolean drawBothSidesLight = Boolean.FALSE;
	@Override
	protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
		boolean draw =  super.drawChild(canvas, child, drawingTime);
		if(drawBothSidesLight){
		drawBothSidesLight(canvas, child);
		}
		return draw;
	}

	/**
	 * 绘制左右两侧边缘顶层光效果，左右更多的效果
	 * @param canvas
	 * @param child
	 */
	private void drawBothSidesLight(Canvas canvas, View child) {
		if(getChildCount()==1){
			if(getScrollX()>0){
				//draw-left
				Rect _rect = canvas.getClipBounds();
				ColorDrawable left = new ColorDrawable(Color.RED);
				left.setBounds(_rect.left, _rect.top, _rect.right-(screenWidth-100), _rect.bottom);
				left.draw(canvas);
			}
			if((getScrollX()+getWidth())==child.getWidth()){
				//draw-right-nodraw
			}else{
				//drawing-ring
				ColorDrawable right = new ColorDrawable(Color.RED);
				Rect _rect = canvas.getClipBounds();
				right.setBounds(screenWidth-100+getScrollX(),_rect.top, _rect.right, _rect.bottom);
				right.draw(canvas);
			}
		}
	}
	
	

	// 滑动距离及坐标
	private float xDistance, yDistance, xLast, yLast;

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			xDistance = yDistance = 0f;
			xLast = ev.getX();
			yLast = ev.getY();
			break;
		case MotionEvent.ACTION_MOVE:
			final float curX = ev.getX();
			final float curY = ev.getY();

			xDistance += Math.abs(curX - xLast);
			yDistance += Math.abs(curY - yLast);
			xLast = curX;
			yLast = curY;

			if (xDistance > yDistance) {
				return false;
			}
		}

		return super.onInterceptTouchEvent(ev);
	}

	public boolean isDrawBothSidesLight() {
		return drawBothSidesLight;
	}

	public void setDrawBothSidesLight(boolean drawBothSidesLight) {
		this.drawBothSidesLight = drawBothSidesLight;
		postInvalidate();
	}


}
