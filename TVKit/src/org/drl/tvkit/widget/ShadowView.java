package org.drl.tvkit.widget;

import org.drl.tvkit.util.L;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * 倒影图片组件
 * 
 * @author dai.rui.lin
 * 
 */
public class ShadowView extends ImageView {
	private String TAG = "ShadowView";
	public ShadowView(Context context) {
		super(context);
		init();
	}
	private MovieView mMovieView;

	public ShadowView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public ShadowView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
	}
//	@SuppressLint("DrawAllocation")
//	@Override
//	protected void onDraw(Canvas canvas) {
//		if (canvas != null) {
//			canvas.setDrawFilter(new PaintFlagsDrawFilter(0,
//					Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG));
////			LinearGradient shader = new LinearGradient(0,
////					0 , 0, getHeight(),
////					Color.TRANSPARENT, Color.BLACK, TileMode.REPEAT);
////			paintShadow.setShader(shader);
////			paintShadow.setXfermode(new PorterDuffXfermode(
////					Mode.DST_IN));
////			canvas.drawRect(0,5,getWidth(), getHeight(), paintShadow);
//		}
//		super.onDraw(canvas);
//	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		StringBuilder sb = new StringBuilder("onSizeChanged:W*H=");
		sb.append(w);
		sb.append("*");
		sb.append(h);
		sb.append(",oldw*oldh=");
		sb.append(oldw);
		sb.append('*');
		sb.append(oldh);
		L.i(TAG, sb.toString());
		if(w>0&&h>0){
			if(mMovieView!=null){
				mMovieView.reloadShadow();
			}
		}
	}


}
