package org.drl.tvkit.widget;

import org.drl.tvkit.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

/**
 * @author Darry Ring
 * 
 */
public class MovieLayout extends RelativeLayout {

	private Animation scaleSmallAnimation;
	private Animation scaleBigAnimation;

	public MovieLayout(Context context) {
		super(context);
		init();
	}

	public MovieLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public MovieLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	protected void init() {
	}
	@Override
	public void setSelected(boolean selected) {
		if(isSelected()&&selected){
			return;
		}
		super.setSelected(selected);
		clearAnimation();
		if (selected) {
			bringToFront();
			zoomOut();
		} else {
			zoomIn();
		}
	}
	private void zoomIn() {
		if (scaleSmallAnimation == null) {
			scaleSmallAnimation = AnimationUtils.loadAnimation(getContext(),
					R.anim.anim_scale_small);
		}
		startAnimation(scaleSmallAnimation);
	}
	@Override
	protected void onAnimationEnd() {
		super.onAnimationEnd();
		postInvalidate();
	}
	
//	@Override
//	protected void onAttachedToWindow() {
//		super.onAttachedToWindow();
//		
//		
//		
//		AnimatorSet set = new AnimatorSet();
//		set.playTogether(
//		    ObjectAnimator.ofFloat(this, "rotationY", 20, 0,0),
//		    ObjectAnimator.ofFloat(this, "translationX", 100, 0,0),
//		    ObjectAnimator.ofFloat(this, "alpha",0, 1, 1)
//		);
//		set.setDuration(5 * 1000).start();
//	}

	private void zoomOut() {
		if (scaleBigAnimation == null) {
			scaleBigAnimation = AnimationUtils.loadAnimation(getContext(),
					R.anim.anim_scale_big);
			
			if(scaleBigAnimation!=null){
				scaleBigAnimation.setAnimationListener(new AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) {
					}
					@Override
					public void onAnimationRepeat(Animation animation) {
					}
					@Override
					public void onAnimationEnd(Animation animation) {
						View _rootView = getRootView();
						if(_rootView!=null){
							_rootView.postInvalidateDelayed(30);
						}
					}
				});
			}
		}
		startAnimation(scaleBigAnimation);
	}

}
