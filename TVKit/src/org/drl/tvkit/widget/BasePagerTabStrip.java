package org.drl.tvkit.widget;


/**
 * @author dai.rui.lin
 *
 */
public abstract interface BasePagerTabStrip {
	
	public void setViewPager(VooleViewPager pager);

}
