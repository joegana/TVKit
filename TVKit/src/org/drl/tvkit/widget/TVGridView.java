package org.drl.tvkit.widget;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.GridView;

/**
 * @author Darry Ring
 * 
 */
public class TVGridView extends GridView {

	public TVGridView(Context context) {
		super(context);
	}

	public TVGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TVGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		final int  oldSelect =getSelectedItemPosition();
		boolean mRm =  super.onKeyDown(keyCode, event);
		final int newSelect = getSelectedItemPosition();
		if(event.getAction()==KeyEvent.ACTION_DOWN){
			switch(keyCode){
			case KeyEvent.KEYCODE_DPAD_UP:
				if(newSelect==oldSelect){
					if(!mPageAnimatorSet.isRunning()){
	            		mPageAnimatorSet.playTogether(
	                    	    ObjectAnimator.ofFloat(this, "translationY", 0, 80,0),
	                    	    ObjectAnimator.ofFloat(this, "scaleX", 1, 0.95f,1),
	                    	    ObjectAnimator.ofFloat(this, "scaleY", 1, 0.95f,1)
	                    	);
	                    mPageAnimatorSet.setDuration(500).start();
            	  }
				}
				break;
			case KeyEvent.KEYCODE_DPAD_DOWN:
				int _count =  getAdapter().getCount();
				if(newSelect==oldSelect||newSelect>_count){
					if(!mPageAnimatorSet.isRunning()){
	            		mPageAnimatorSet.playTogether(
	                    	    ObjectAnimator.ofFloat(this, "translationY", 0, -80,0),
	                    	    ObjectAnimator.ofFloat(this, "scaleX", 1, 0.95f,1),
	                    	    ObjectAnimator.ofFloat(this, "scaleY", 1, 0.95f,1)
	                    	);
	                    	mPageAnimatorSet.setDuration(500).start();
	            	}
				}
				break;
			}
		}
		
		return mRm;
	}
	private AnimatorSet mPageAnimatorSet = new AnimatorSet();
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();

	}
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
	}


}
