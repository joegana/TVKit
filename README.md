#TVKit
#项目为Android TV Box上运行，其它Android设备运行效果会降低
# android tv开发，View组件边缘在选中的时候显示光效果

##实现View选中后光的效果，必须设置View组件外层的ViewGroup ：android:clipChildren="false"或者代码中 ViewGroup.setClipChildren(false);
##实现View倒影管理，单线程动态创建View组件的倒影。
###
![image](http://git.oschina.net/hljdrl/TVKit/raw/master/TVKit/screenshot/tvkit_001.png)
![image](http://git.oschina.net/hljdrl/TVKit/raw/master/TVKit/screenshot/tvkit_002.png)
